window.gameUI = window.gameUI || [];

gameUI.push(`
<div class="fixed-bottom row mb-3">
        <div class="mx-auto">
            <div class="row">
                <div class="inventory-slot empty border border-info alert-info col">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">1</p>
                </div>
                <div class="inventory-slot empty border border-info alert-info col">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">2</p>
                </div>
                <div class="inventory-slot empty border border-info alert-info col">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">3</p>
                </div>
                <div class="inventory-slot border border-info alert-info col inventory-potion-blue">
                    <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99</p>
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">4</p>
                </div>
                <div class="inventory-slot border border-info alert-info col inventory-potion-red">
                    <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99</p>
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">5</p>
                </div>
                <div class="inventory-slot border border-info alert-info col inventory-potion-green">
                    <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99</p>
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">6</p>
                </div>
            </div>
            <div class="row">
                <div class="inventory-slot border border-info alert-info col action-hearth-break">
                    <div class="inventory-slot"
                         style="background-color: #000000;z-index: 1; opacity: .7;position: absolute;">
                    </div>
                    <p style="position:absolute;font-size: 34px;color: #e2e6ea; opacity: 1;z-index: 10"
                       class="text-center w-100" data-count-timer="60">60</p>
                    <p class="float-right m-0"
                       style="position: absolute;right: 41%;bottom: -10px; color: #e2e6ea; z-index: 15">q</p>
                </div>
                <div class="inventory-slot border border-info alert-info col action-piu">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px; color: #e2e6ea">w</p>
                </div>
                <div class="inventory-slot border border-info alert-info col action-fireball">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px; color: #e2e6ea">e</p>
                </div>
                <div class="inventory-slot border border-info alert-info col action-shards">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px; color: #e2e6ea">r</p>
                </div>
                <div class="inventory-slot empty border border-info alert-info col">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">t</p>
                </div>
                <div class="inventory-slot empty border border-info alert-info col">
                    <p class="float-right m-0" style="position: absolute;right: 41%;bottom: -10px">y</p>
                </div>
            </div>
        </div>
    </div>
`);