window.gameUI = window.gameUI || [];

gameUI.push(`
<div class="row" style="position: absolute;left: 36%;width: 635px;top:10px;">
       <div class="">
           <div class="alert-primary boss_icon" style="width: 64px;height: 64px"></div>
       </div>
       <div class="col pl-0">
           <h4>Большой страшый босс</h4>
           <div class="progress mt-1  border border-info" style="height: 30px;">
               <span class="mx-auto" style="position: absolute; left:33%;font-size: 20px">99999/99999</span>
               <div class="progress-bar bg-danger text-center" role="progressbar"
                    style="width:75%;height: 30px" aria-valuenow="100" aria-valuemin="0" aria-valuemax="99999">
               </div>
           </div>
           <div class="row mt-2">
               <div class="border border-info inventory-slot buff_icon" style="width: 18px;height: 18px"></div>
               <div class="border border-info inventory-slot buff_icon" style="width: 18px;height: 18px"></div>
               <div class="border border-info inventory-slot buff_icon" style="width: 18px;height: 18px"></div>
               <div class="border border-info inventory-slot buff_icon" style="width: 18px;height: 18px"></div>
               <div class="border border-info inventory-slot buff_icon" style="width: 18px;height: 18px"></div>
           </div>
       </div>
   </div>
`);