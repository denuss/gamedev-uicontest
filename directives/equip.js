window.directives = window.directives || [];

directives.push(
    `
<div class="mymodal" role="document" id="js-equip" style="width: 450px; left: 541px; top: 381px;">
    <div class="modal-content">
    <div class="modal-header">
    <h5 class="modal-title">Вещи</h5>
    <button type="button" class="close" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body row">
    <div class="col-6 text-left">
    <div class="row mx-auto">
    <div class="border float-left btn-primary alert alert-info inventory-slot col-auto inventory-upg-helmet"></div>
    <div class="col">
    <div class="row" style="height: 28px;">
    <p class="m-0 text-justify" style="line-height: 12px">длинное название предмета aaaaaa</p>
</div>
<div class="row">
    <p class="m-0 small text-black-50">шлем</p>
    </div>
    </div>
    </div>
    <div class="row mx-auto">
    <div class="border float-left btn-primary alert alert-info inventory-slot empty col-auto"></div>
    <div class="col">
    <div class="row" style="height: 28px;">
        <p class="m-0 text-justify" style="line-height: 12px">короткое название</p>
    </div>
<div class="row">
    <p class="m-0 small text-black-50">аксессуар</p>
</div>
</div>
</div>
<div class="row mx-auto">
    <div class="border float-left btn-primary alert alert-info inventory-slot col-auto inventory-axe"></div>
    <div class="col">
    <div class="row" style="height: 28px;">
    <p class="m-0 text-justify" style="line-height: 12px">название</p>
    </div>
    <div class="row">
    <p class="m-0 small text-black-50">правая рука</p>
</div>
</div>
</div>
<div class="row mx-auto">
    <div class="border float-left btn-primary alert alert-info inventory-slot empty col-auto "></div>
    <div class="col">
    <div class="row" style="height: 28px;">
    <p class="m-0 text-justify" style="line-height: 12px">название</p>
    </div>
    <div class="row">
    <p class="m-0 small text-black-50">ботинки</p>
    </div>
    </div>
    </div>
    </div>
    <div class="col-6 text-right">
    <div class="row float-right mx-auto">
    <div class="col">
    <div class="row" style="height: 28px;">
    <p class="m-0 w-100 text-justify" style="line-height: 12px">длинное название предмета aaaaaa</p>
</div>
<div class="row">
    <p class="m-0 w-100 small text-black-50">амулет</p>
    </div>
    </div>
    <div class="border float-left btn-primary alert alert-info inventory-slot empty col-auto"></div>
    </div>
    <div class="row float-right mx-auto">
    <div class="col">
    <div class="row" style="height: 28px;">
    <p class="m-0 w-100 text-justify" style="line-height: 12px">короткое название</p>
</div>
<div class="row">
    <p class="m-0 w-100 small text-black-50">туловище</p>
    </div>
    </div>
    <div class="border float-left btn-primary alert alert-info inventory-slot col-auto inventory-armor"></div>
    </div>
    <div class="row float-right mx-auto">
    <div class="col">
    <div class="row" style="height: 28px;">
    <p class="m-0 w-100 text-justify" style="line-height: 12px">название</p>
    </div>
    <div class="row">
    <p class="m-0 w-100 small text-black-50">левая рука</p>
</div>
</div>
<div class="border float-left btn-primary alert alert-info inventory-slot col-auto inventory-upg-shield"></div>
    </div>
    <div class="row float-right mx-auto">
    <div class="col">
    <div class="row" style="height: 28px;">
    <p class="m-0 w-100 text-justify" style="line-height: 12px">название</p>
    </div>
    <div class="row">
    <p class="m-0 w-100 small text-black-50">аксессуар</p>
    </div>
    </div>
    <div class="border float-left btn-primary alert alert-info inventory-slot empty col-auto"></div>
    </div>
    </div>
    </div>
    </div>
    </div>
`);