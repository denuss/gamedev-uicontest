window.directives = window.directives || [];

directives.push(`
<div class="mymodal" role="document" style="width: 600px; left: 643px; top: 129px;" id="js-clan">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Гильдия\\Клан</h5>
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" js-tab-group="guild" id="guild-main-tab">
            Главная
            рейтинг гильдии,
            ее "прокачанность"
            описание
            текущие союзы и враги
        </div>
        <div class="modal-body" js-tab-group="guild" id="guild-list-tab" style="display: none">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">звание</th>
                    <th scope="col">ник</th>
                    <th scope="col">вклад</th>
                    <th scope="col">налог</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        царь
                    </td>
                    <td>
                        Петр I
                    </td>
                    <td>
                        9000
                    </td>
                    <td>
                        <input type="number" min="0" max="100" value="0">
                    </td>
                </tr>
                <tr>
                    <td>
                        халоп
                    </td>
                    <td>
                        вася
                    </td>
                    <td>
                        9000000
                    </td>
                    <td>
                        <input type="number" min="0" max="100" value="99">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-body" js-tab-group="guild" id="guild-skills-tab" style="display: none">
            тут список навыков гильдии\\клана.
        </div>
        <div class="modal-body" js-tab-group="guild" id="guild-control-tab" style="display: none">
            Управление
            тут всякие кнопочки и переключатели, добавляем ранги, заключаем союзы
            сделаю если успею
        </div>
        <div class="modal-footer justify-content-start">
            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" js-tab-switcher="#guild-main-tab" class="btn btn-secondary">Главная</button>
                <button type="button" js-tab-switcher="#guild-list-tab" class="btn btn-secondary">Список</button>
                <button type="button" js-tab-switcher="#guild-skills-tab" class="btn btn-secondary">Навыки</button>
                <button type="button" js-tab-switcher="#guild-control-tab" class="btn btn-secondary">Управление</button>
            </div>
        </div>
    </div>
</div>
    `);