window.directives = window.directives || [];

directives.push(
    `<div class="mymodal" role="document" id="js-inventory" style="width: 600px; left: 1290px; top: 431px;">
    <div class="modal-content">
        <div class="modal-header">
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-backpack-slot active">
                <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99/99</p>
            </div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-backpack-slot">
                <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99/99</p>
            </div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-backpack-slot empty">
            </div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-backpack-slot empty">
            </div>
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-potion-blue">
                <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99</p>
            </div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-armor"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-potion-red">
                <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99</p>
            </div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-axe"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-potion-blue">
                <p class="float-right m-0" style="position: absolute;right: 0px;bottom: 0px">99</p>
            </div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-gemBlue"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-tome"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot inventory-upg-shield"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
            <div class="border float-left btn-primary alert alert-info inventory-slot empty"></div>
        </div>
        <div class="modal-footer text-center">
            <div class="justify-content-center col-3 pr-0">
                <div class="border btn-primary alert alert-info p-0 mx-auto" style="width: 48px;height: 48px;"><h1>
                    €</h1></div>
                <h4 class="mr-2">12</h4>
            </div>
            <div class="justify-content-center col-3 pr-0 mx-auto">
                <div class="border btn-primary alert alert-info p-0 mx-auto" style="width: 48px;height: 48px"><h1>$</h1>
                </div>
                <h4 class="mr-2">12 345 678</h4>
            </div>
            <div class="justify-content-center col-3 pr-0">
                <div class="border btn-primary alert alert-info p-0 mx-auto" style="width: 48px;height: 48px"><h1>£</h1>
                </div>
                <h4 class="mr-2 mx-auto">12 345</h4>
            </div>
            <div class="justify-content-center col-3 pr-0">
                <div class="border btn-primary alert alert-info p-0 mx-auto" style="width: 48px;height: 48px"><h1>₽</h1>
                </div>
                <h4 class="mr-2 mx-auto">12 345 678</h4>
            </div>
        </div>
    </div>
</div>`
);
