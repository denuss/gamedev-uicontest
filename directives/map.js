window.gameUI = window.gameUI || [];

gameUI.push(`
    <div class="float-right border border-info alert alert-primary p-2" style="width: 256px;height: 256px">
        <div style="position: absolute; top: 5px;left: -28px;">
            <button class="btn btn-block">+</button>
            <button class="btn btn-block">-</button>
            <div class="btn-group mt-1" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="input-group-prepend dropdown-item p-1">
                        <div class="input-group-text w-100">
                            <input type="checkbox" id="exampleCheck1"  >
                            <label class="form-check-label w-100 h-100 text-left" for="exampleCheck1">опция 1</label>
                        </div>
                    </div>
                    <div class="input-group-prepend dropdown-item p-1">
                        <div class="input-group-text w-100">
                            <input type="checkbox" id="exampleCheck2"  >
                            <label class="form-check-label w-100 h-100 text-left" for="exampleCheck2">опция 2</label>
                        </div>
                    </div>
                    <div class="input-group-prepend dropdown-item p-1">
                        <div class="input-group-text w-100">
                            <input type="checkbox" id="exampleCheck3"  >
                            <label class="form-check-label w-100 h-100 text-left" for="exampleCheck3">опция 3</label>
                        </div>
                    </div>
                    <div role="separator" class="dropdown-divider"></div>
                    <div class="input-group-prepend dropdown-item p-1">
                        <div class="input-group-text w-100">
                            <input type="checkbox" id="exampleCheck4"  >
                            <label class="form-check-label w-100 h-100 text-left" for="exampleCheck4">опция 4</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mx-auto w-100 h-100" style="background-color: #1c7430"></div>
        <div class="mx-auto w-50 p-1 border border-info alert alert-primary text-center">111:111</div>
    </div>
`);