window.gameUI = window.gameUI || [];

gameUI.push(`
       <div class="fixed-bottom">
        <div class="col-auto float-right pl-0 pb-3">
            <button class="btn btn-dark  float-right" data-toggle="collapse" role="button" aria-expanded="false"
                    data-target="#collapsePanel" aria-controls="collapsePanel">
                &lt;&lt;
            </button>
        </div>
        <div class="col-4  float-right collapse pr-1" id="collapsePanel">
            <div class="row mr-1 mb-1 float-right" role="group" aria-label="First group">
                <button type="button" class="btn btn-secondary" modal-toggler="#js-stats" style="width: 121px;">
                    Параметры
                </button>
                <button type="button" class="btn btn-secondary" modal-toggler="#js-inventory" style="width: 121px;">
                    Инвентарь
                </button>
                <button type="button" class="btn btn-secondary" modal-toggler="#js-equip" style="width: 121px;">
                    Вещи
                </button>

            </div>

            <div class="row mr-1 mb-1 float-right" role="group" aria-label="First group">
                <button type="button" class="btn btn-secondary" disabled modal-toggler="" style="width: 121px;">
                    Способности
                </button>
                <button type="button" class="btn btn-secondary" disabled modal-toggler="" style="width: 121px;">
                    Квесты
                </button>
  <!--              <button type="button" class="btn btn-secondary" modal-toggler="#js-clan" style="width: 121px;">
                    Клан
                </button>
                <button type="button" class="btn btn-secondary" modal-toggler="" style="width: 121px;">
                    Кнопка
                </button>-->
                <button type="button" class="btn btn-secondary" disabled modal-toggler="" style="width: 121px;">
                    Крафт
                </button>
            </div>
        </div>

    </div>
`);