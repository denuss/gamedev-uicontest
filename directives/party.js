window.gameUI = window.gameUI || [];

gameUI.push(`
    <div class="col" style="position: absolute;top:25%">
        <div class="row border border-info alert alert-info mt-1 ml-1 p-1" style="width: 350px;">
            <div class="btn btn-dark btn-sm" style="position: absolute;right: 2%;">&gt&gt</div>
            <div class="col-2 p-0">
                <div class="alert-primary party_member_icon" style="width: 64px;height: 64px"></div>
            </div>
            <div class="col">
                <h4>Очень длинное имя</h4>
                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left:33%">99999/99999</span>
                    <div class="progress-bar bg-danger text-center" role="progressbar"
                         style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar text-center" role="progressbar"
                         style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar bg-warning" role="progressbar"
                         style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>
            </div>
        </div>
        <div class="row border border-info alert alert-info mt-1 ml-1 p-1" style="width: 350px;">
            <div class="btn btn-dark btn-sm" style="position: absolute;right: 2%;">&gt&gt</div>
            <div class="col-2 p-0">
                <div class="alert-primary party_member_icon" style="width: 64px;height: 64px"></div>
            </div>
            <div class="col">
                <h4>Очень длинное имя</h4>
                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left:33%">99999/99999</span>
                    <div class="progress-bar bg-danger text-center" role="progressbar"
                         style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar text-center" role="progressbar"
                         style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar bg-warning" role="progressbar"
                         style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>
            </div>
        </div>
        <div class="row border border-info alert alert-info mt-1 ml-1 p-1" style="width: 350px;">
            <div class="btn btn-dark btn-sm" style="position: absolute;right: 2%;">&gt&gt</div>
            <div class="col-2 p-0">
                <div class="alert-primary party_member_icon" style="width: 64px;height: 64px"></div>
            </div>
            <div class="col">
                <h4>Очень длинное имя</h4>
                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left:33%">99999/99999</span>
                    <div class="progress-bar bg-danger text-center" role="progressbar"
                         style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar text-center" role="progressbar"
                         style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar bg-warning" role="progressbar"
                         style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>
            </div>
        </div>
        <div class="row border border-info alert alert-info mt-1 ml-1 p-1" style="width: 350px;">
            <div  class="btn btn-dark btn-sm" style="position: absolute;right: 2%;">&gt&gt</div>
            <div class="col-2 p-0">
                <div class="alert-primary party_member_icon" style="width: 64px;height: 64px"></div>
            </div>
            <div class="col">
                <h4>Очень длинное имя</h4>
                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left:33%">99999/99999</span>
                    <div class="progress-bar bg-danger text-center" role="progressbar"
                         style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                    <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar text-center" role="progressbar"
                         style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>

                <div class="progress mt-1">
                <span class="mx-auto" style="position: absolute; left: 33%">99999/99999</span>
                    <div class="progress-bar bg-warning" role="progressbar"
                         style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="99999">
                    </div>
                </div>
            </div>
        </div>
    </div>
`);