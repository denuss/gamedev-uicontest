window.directives = window.directives || [];

directives.push(`
<div class="mymodal" role="document" style="width: 450px; left: 20px; top: 138px;" id="js-stats">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Параметры</h5>
            <button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body row">
            <div class="col-6 border-right">
                <div class="row border-bottom">
                    <div class="col-7">Сила</div>
                    <div class="col-5 text-right">
                        999+999
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">Ловкость</span>
                    <div class="col-5 text-right">
                        999+999
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">Интеллект</span>
                    <div class="col-5 text-right">
                        999+999
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">Харизма</span>
                    <div class="col-5 text-right">
                        99+99
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">Выносливость</span>
                    <div class="col-5 text-right">
                        9+9
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">Удача</span>
                    <div class="col-5 text-right">
                        99
                    </div>
                </div>
            </div>
            <div class="col-6 border-left">
                <div class="row border-bottom">
                    <div class="col-4">атака</div>
                    <div class="col-8 text-right">
                        999-999+999
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-4">м.атака</span>
                    <div class="col-8 text-right">
                        999-999+999
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">защита</span>
                    <div class="col-5 text-right">
                        999+999
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">м.защита</span>
                    <div class="col-5 text-right">
                        99+99
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">крит шанс</span>
                    <div class="col-5 text-right">
                        9+9
                    </div>
                </div>
                <div class="row border-bottom">
                    <span class="col-7">скорость</span>
                    <div class="col-5 text-right">
                        99
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`);