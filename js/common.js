function injectModals() {
    window.directives = window.directives || [];
    let $inc = $("div[include]");
    directives.forEach(function (el) {
        let $el = $(el);
        $inc.append($el);
    });

    let modals = $(".mymodal");
    modals.draggable({
        handle: ".modal-header"
    });
    modals.find("button.close").click(function (el) {
        let $el = $(el.currentTarget);
        $el.closest(".mymodal").hide();
    });

    modals.hide();
}

function initTogglers() {
    $('[modal-toggler]').each(function (indx, el) {
        let $butt = $(el);
        let attr = $butt.attr("modal-toggler");
        if (attr && attr.length) {
            let $modal = $(attr);
            $butt.on("click", function () {
                $modal.toggle();
            });
        }
    });
}

function initTooltips() {
    $('.inventory-slot:not(.empty)').tooltip({
        html: true,
        placement: "right",
        title: `
                   <h3>Название</h3>
                   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Ut pellentesque leo est, a ultrices dolor porta bibendum.</p>
                `
    });
}

function injectBody() {
    window.gameUI = window.gameUI || [];
    let $cont = $(".container-fluid");
    gameUI.forEach(function (el) {
        let $el = $(el);
        $cont.append($el);
    })
}

function initTimers() {
    $('[data-count-timer]').each(function (indx, el) {
        let $el = $(el);
        let time = $el.attr("data-count-timer");
        $el.html(time);
        let counter = time;
        setTimeout(function tick() {
            if (counter < 0) {
                counter = time;
            }
            $el.html(counter--);
            setTimeout(tick, 1000);
        }, 1000)
    });
}

function initTabSwitchers() {
    $("[js-tab-switcher]").each(function (indx, el) {
        let $el = $(el);
        let $tab = $($el.attr("js-tab-switcher"));
        let $tabGroup = $(`[js-tab-group='${$tab.attr("js-tab-group")}']`);
        $el.on("click", function () {
            $tabGroup.hide();
            $tab.show();
        })
    });
}
